#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import *
from time import sleep, strftime
from datetime import datetime
from Adafruit_I2C import Adafruit_I2C
from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

import smbus
import subprocess
import re
import os

lcd = Adafruit_CharLCDPlate(busnum = 1)

cmd = "ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1"
cmd2 = "ip addr show wlan0 | grep inet | awk '{print $2}' | cut -d/ -f1"
#cmd3 = "/sbin/wpa_supplicant -Dwext -iwlan0 -c/etc/wpa_supplicant/wpa_supplicant.conf "
cmd3 = "ifdown wlan0 && ifup wlan0"
cmd4 = "ifdown wlan0"
freespace = "df /dev/root --si | awk '{print $4}' | egrep -iv Avail"
percused = "df /dev/root --si | awk '{print $5}' | egrep -iv Use | sed s/%//"
name = "hostname"
load = "uptime | sed s/,//g| awk '{print $9}'"
psign = unichr(37)
sigqual = "sudo iwconfig wlan0 | grep Quality | awk '{print $2}'  | sed 's/.*=//g'"
localcount = 0
count = 0
count2 = 0
count3 = 0

lcd.begin(16,1)

def findThisProcess(process_name):
	ps = subprocess.Popen("ps -eaf | grep -v grep | grep "+process_name, shell=True, stdout=subprocess.PIPE)
	output = ps.stdout.read()
	ps.stdout.close()
	ps.wait()
	if output != "":
		return True
	else:
		return False

# runs the command and does return a value
def run_cmd(cmd):
	p = Popen(cmd, shell=True, stdout=PIPE)
	output = p.communicate()[0]
	return output

# runs the command, but does not return a value
def run_cmd_q(cmd):
	p = Popen(cmd, shell=True, stdout=PIPE)
	output = p.communicate()[0]
	print("Done")

def motion_screen():
	lcd.clear()
	sload=run_cmd(load)
	hostname = run_cmd(name).strip()
	lcd.message("Toggle MOTION \n state" )



def ip_address():
	ipaddr = run_cmd(cmd)
	wipaddr = run_cmd(cmd2)
	lcd.clear()
	if ipaddr == "":
		lcd.message('eth0 disconnect\n')
	else:
		lcd.message('E%s\n' % ( ipaddr ) )
	if wipaddr == "":
		lcd.message('wlan0 disconnect')
	else:
		lcd.message('W%s' % ( wipaddr ) )

def waitforup():
	wipaddr = run_cmd(cmd2)
	count = 0
	while wipaddr == "":
		wipaddr = run_cmd(cmd2)
		count += 1
		if count == 25:
			break
	lcd.clear()
	if wipaddr == "":
		lcd.message('wlan0 disconnect')
	else:
		lcd.message('Wireless IP\n %s' % ( wipaddr ) )
	return

def wlan(d):
	#print(d)
	if d == 1:
		run_cmd_q(cmd3)
		#print("Upping")
		waitforup()
	elif d == 2:
		run_cmd_q(cmd4)
		#print("Downing")
	else:
		return()	

def bgcolor():
	motionstatus = findThisProcess("motion")
	wipaddr = run_cmd(cmd2)
	if not motionstatus:
		lcd.backlight(lcd.RED)
	elif wipaddr == "":
		lcd.backlight(lcd.BLUE)
	else:
		lcd.backlight(lcd.GREEN)
	

def main_screen():
	lcd.clear()
	free = run_cmd(percused)
	percfree = 100 - int(free)
	diskfree = run_cmd(freespace).strip()
	pfree=str(percfree).strip()
	motionstatus = findThisProcess("motion")
	bgcolor()
	lcd.message("Dsk Free %s %s%s\nMotion = %s" % (diskfree,pfree,psign,motionstatus))

def signal_screen():
        lcd.backlight(lcd.OFF)
        lcd.clear()
        sload=run_cmd(load)
        signal = run_cmd(sigqual)
        lcd.message("Load %s \nSignal  %s" % (sload,signal))

if __name__ == '__main__':
    while 1:
		main_screen()
		refresh = 0
		while refresh < 2000:
			
			if (lcd.buttonPressed(lcd.LEFT)):
				bgcolor()
				lcd.clear()
				lcd.message("Reseting LCD\nScreen & Buttons")
				sleep(1)
				lcd.noDisplay()
				run_cmd_q("/home/webide/repositories/lcd/big_restart_screen.sh")	
				exit()

			if (lcd.buttonPressed(lcd.UP)):
				lcd.clear()
				bgcolor()
				wipaddr = run_cmd(cmd2)
				if wipaddr == "":				
					lcd.message("WLAN0 is DOWN\n")
				else:
					lcd.message("WLAN0 is UP\n")
				lcd.message("SEL to Change")
				count = 0
				while 1:
					if count < 2000:
						if (lcd.buttonPressed(lcd.SELECT)):
							if wipaddr == "":
								lcd.clear()
								lcd.message("WLAN0\nTransistion UP")
								wlan(1)
							else:
								lcd.clear()
								lcd.message("WLAN0\nTransistion DOWN")
								wlan(2)
						if (lcd.buttonPressed(lcd.LEFT)):
							break
						if (lcd.buttonPressed(lcd.UP)):
							lcd.clear()
							bgcolor()
							motionstatus = findThisProcess("motion")
							lcd.message("Motion = %s \n" % (motionstatus))
							lcd.message("Select to change")
							count2 = 0
							while 1:
								if count2 < 2000:
									if (lcd.buttonPressed(lcd.LEFT)):
										lcd.clear()
										break
									if (lcd.buttonPressed(lcd.SELECT)):
										lcd.clear()
										if motionstatus == False:
											lcd.clear()
											lcd.message("Starting Motion")
											run_cmd_q("motion")
											sleep(1.5)
											break
										else:
											lcd.clear()
											lcd.message("Stopping Motion")
											run_cmd("/etc/init.d/motion stop")
											break		
									if (lcd.buttonPressed(lcd.UP)):
										lcd.clear()
										bgcolor()
										signal = run_cmd(sigqual)
										lcd.message("Signal %s" % (signal))
										lcd.message("Select to test")
										secondcount = 0
										while 1:
											if count3 < 1000:
												print(count3)
												if (lcd.buttonPressed(lcd.SELECT)):
													print("Select")
													while localcount < 10:
														lcd.backlight(lcd.ON)
														lcd.clear()
														signal = run_cmd(sigqual)
														lcd.message("Signal  %s" % (signal))
														sleep(1)
														localcount += 1
													break
												count3 += 1	
											else:
												count2 = 2000
												break
									if (lcd.buttonPressed(lcd.DOWN)):
										count2 = 2000
									count2 += 1
								else:
									count = 2000
									break
							main_screen()	
						count += 1
					else:
						break
				sleep(1.5)
				main_screen()			

			if (lcd.buttonPressed(lcd.DOWN)):
				lcd.clear()
				bgcolor()
				lcd.message("Dwn-Rbt Up-Halt\n")
				lcd.message("L-Back R-Scn off")
				count = 0
				while 1:
					if count < 2000:
						if (lcd.buttonPressed(lcd.DOWN)):
							lcd.clear()
							run_cmd_q("/home/webide/repositories/lcd/reboot_script.sh")
							run_cmd_q("reboot")
							exit
						if (lcd.buttonPressed(lcd.LEFT)):
							break
						if (lcd.buttonPressed(lcd.RIGHT)):
							signal_screen()
							exit()
						if (lcd.buttonPressed(lcd.UP)):
							lcd.clear()
							lcd.message("System Halt!\nCycle Pwr to Up")
							run_cmd_q("halt")
							exit()
						count += 1
					else:
						break
				main_screen()

			if (lcd.buttonPressed(lcd.RIGHT)):
				main_screen()

			if (lcd.buttonPressed(lcd.SELECT)):
				lcd.clear()
				bgcolor()
				ip_address()
				count = 0
				while 1:
					if count < 2000:
						if (lcd.buttonPressed(lcd.LEFT)):
							lcd.clear()
							break
						if (lcd.buttonPressed(lcd.SELECT)):
							lcd.clear()
							lcd.message("Bouncing Relay")
							run_cmd_q("/home/webide/repositories/lcd/control_relay.py")
							break
						count += 1
					else:
						break
				main_screen()
			refresh += 1
			print(refresh)
