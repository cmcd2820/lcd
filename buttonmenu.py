#!/usr/bin/python

from time import sleep
from Adafruit_I2C import Adafruit_I2C
from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

import smbus


# initialize the LCD plate
# use busnum = 0 for raspi version 1 (256MB) and busnum = 1 for version 2
lcd = Adafruit_CharLCDPlate(busnum = 1)

# clear display
lcd.clear()
# hello!
lcd.message("All Hail the Pi\nWorking")
#sleep(1)
#lcd.message(".")
#sleep(1)
#lcd.message(".")
#sleep(1)
#lcd.message(".")
#sleep(1)
#lcd.message(".")
#sleep(4)
lcd.backlight(lcd.ON)


#menu_items = tuple(open(menuitems, 'r'))

print(menu_items)
#exit()

while 1:
	if (lcd.buttonPressed(lcd.LEFT)):
		lcd.clear()
		lcd.message("Red Red Wine")
		lcd.backlight(lcd.ON)

	if (lcd.buttonPressed(lcd.UP)):
		lcd.clear()
		lcd.message("Sita Sings \nthe blues")
		lcd.backlight(lcd.ON)

	if (lcd.buttonPressed(lcd.DOWN)):
		lcd.clear()
		lcd.message("I see fields\nof green");
		lcd.backlight(lcd.ON)

	if (lcd.buttonPressed(lcd.RIGHT)):
		lcd.clear()
		lcd.message("Purple mountain\n majesties");
		lcd.backlight(lcd.ON)

	if (lcd.buttonPressed(lcd.SELECT)):
		lcd.clear()
		lcd.message("Adafruit RGB LCD\nPlate w/Keypad!")
		lcd.backlight(lcd.ON)


