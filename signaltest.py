#!/usr/bin/python
from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import *
from time import sleep, strftime
from datetime import datetime
from Adafruit_I2C import Adafruit_I2C
from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

import smbus
import subprocess
import re
import os

count = 0

load = " uptime | sed s/,//g | sed s/.*ge://g"
sigqual = "sudo iwconfig wlan0 | grep Quality | awk '{print $2}'  | sed 's/.*=//g'"


def findThisProcess(process_name):
	ps = subprocess.Popen("ps -eaf | grep -v grep | grep "+process_name, shell=True, stdout=subprocess.PIPE)
	output = ps.stdout.read()
	ps.stdout.close()
	ps.wait()
	if output != "":
		return True
	else:
		return False

# runs the command and does return a value
def run_cmd(cmd):
	p = Popen(cmd, shell=True, stdout=PIPE)
	output = p.communicate()[0]
	return output

def screen():
	lcd = Adafruit_CharLCDPlate(busnum = 1)
	lcd.backlight(lcd.ON)
	lcd.clear()
	sload=run_cmd(load)
	signal = run_cmd(sigqual)
	lcd.message("Load %s \nSignal  %s" % (sload,signal))

if __name__ == '__main__':
	while count < 5:
	        big_menu = findThisProcess("big_menu.py")
	        if big_menu == False:
			screen()
			sleep(2)
			count += 1
		else:
			exit()
