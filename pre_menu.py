#!/usr/bin/python

import sys 
from subprocess import *
import os
from time import sleep

psid = "ps -eaf | grep -v grep | grep big_menu.py | awk '{print $2}'" 

def run_cmd(cmd):
	p = Popen(cmd, shell=True, stdout=PIPE)
	output = p.communicate()[0]
	return output

# runs the command, but does not return a value
def run_cmd_q(cmd):
	p = Popen(cmd, shell=True, stdout=PIPE)
	output = p.communicate()[0]

if __name__ == '__main__':
	running = run_cmd(psid)
	if running == "":
		run_cmd("/home/webide/repositories/lcd/big_menu.py")
		exit()
	else:
		print(running)
		while running:
			run_cmd("kill "+running)
			print("killing "+ running)
			sleep(1)
			running = run_cmd(psid)
			if running == "":
				break
		print("Done")
	print("Running Menu")
	run_cmd("/home/webide/repositories/lcd/big_menu.py")
	exit()
