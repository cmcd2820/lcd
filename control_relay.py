#!/usr/bin/python

from subprocess import *
from time import sleep, strftime
import os
import subprocess

killcmd = "kill `ps -eaf | grep -v grep | grep bounceRelay | awk '{print $2}' `"

def findThisProcess(process_name):
	ps = subprocess.Popen("ps -eaf | grep -v grep | grep "+process_name, shell=True, stdout=subprocess.PIPE)
        output = ps.stdout.read()
        ps.stdout.close()
        ps.wait()
        if output != "":
                return True
        else:
                return False

# runs the command and does return a value
def run_cmd(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output

if __name__ == '__main__':
	running = findThisProcess("bounceRelay.py")
	if running == True:
		exit()
#		run_cmd(killcmd)
	else:
		run_cmd("/home/webide/repositories/lcd/bounceRelay.py")
		exit()

