#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import *
from time import sleep, strftime
from datetime import datetime
from Adafruit_I2C import Adafruit_I2C
from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

import smbus

lcd = Adafruit_CharLCDPlate(busnum = 1)
count = 0

lcd.begin(16,1)

def run_cmd(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output

if __name__ == '__main__':
	lcd.clear()
	count = 0
	lcd.setCursor(3,1)
	lcd.message("Rebooting           ")
	lcd.setCursor(0,0)
	while count < 20:
		number = int(count)
		lcd.message("|-")
		sleep(0.1)
		count += 1
#	while True:
#		lcd.scrollDisplayRight()
#		sleep(0.1)
