#!/usr/bin/python

import urllib2
from subprocess import *

#This cgi returns the number of "DOWN" opjects in the global status of Argus
website = urllib2.urlopen('http://argus.risd.k12.nm.us/cgi-bin/globalstatus')
#This cgi always returns the globalstatus + 1 (Used for testing)
#website = urllib2.urlopen('http://argus.risd.k12.nm.us/cgi-bin/test1')


# runs the command, but does not return a value
def run_cmd_q(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]


def check_site(url):
	data = url.read().strip().split()
	for item in data:
		if item.isdigit():
        		output = item
	        else:
        	        output = 0
	return int(output)

status = check_site(website)
if int(status) >= 1:
        run_cmd_q("/home/webide/repositories/lcd/relayon.py")
else:
        run_cmd_q("/home/webide/repositories/lcd/relayoff.py")
