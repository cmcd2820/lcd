#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import *
from time import sleep, strftime
from datetime import datetime
from Adafruit_I2C import Adafruit_I2C
from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

import smbus
import subprocess

lcd = Adafruit_CharLCDPlate(busnum = 1)

cmd = "ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1"
cmd2 = "ip addr show wlan0 | grep inet | awk '{print $2}' | cut -d/ -f1"
#cmd3 = "/sbin/wpa_supplicant -Dwext -iwlan0 -c/etc/wpa_supplicant/wpa_supplicant.conf "
cmd3 = "ifconfig wlan0 up"
cmd4 = "ifconfig wlan0 down"
name = "hostname"

lcd.begin(16,1)

def run_cmd(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output

def run_cmd_q(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]

def refresh_screen():
	lcd.clear()
	hostname = run_cmd(name)
	lcd.message(hostname)
	lcd.message(datetime.now().strftime('%b %d  %H:%M:%S\n'))



def ip_address():
	ipaddr = run_cmd(cmd)
	wipaddr = run_cmd(cmd2)
	lcd.clear()
	if ipaddr == "":
		lcd.message('eth0 disconnect\n')
	else:
		lcd.message('IP %s\n' % ( ipaddr ) )
	if wipaddr == "":
		lcd.message('wlan0 disconnect')
	else:
		lcd.message('WIP %s' % ( wipaddr ) )

def wlan(d):
	print(d)
	if d == 1:
		run_cmd_q(cmd3)
		print("Upping")
	elif d == 2:
		run_cmd_q(cmd4)
		print("Downing")
	

if __name__ == '__main__':
    lcd.clear()
    lcd.message("Pi Display   Use\n buttons below")
    while 1:
		if (lcd.buttonPressed(lcd.LEFT)):
			lcd.clear()
			lcd.message("Reseting LCD\nScreen & Buttons")
			sleep(5)
			#lcd.backlight(lcd.OFF)
			lcd.noDisplay()
			run_cmd_q("/home/webide/repositories/lcd/restart_screen.sh")	
			exit()

		if (lcd.buttonPressed(lcd.UP)):
			lcd.clear()
			lcd.message("WLAN0\nTransistion UP")
			wlan(1)
			lcd.backlight(lcd.ON)

		if (lcd.buttonPressed(lcd.DOWN)):
			lcd.clear()
			lcd.message("WLAN0\nTransistion Down")
			wlan(2)
			lcd.backlight(lcd.ON)

		if (lcd.buttonPressed(lcd.RIGHT)):
			lcd.clear()
			lcd.backlight(lcd.ON)
			refresh_screen()

		if (lcd.buttonPressed(lcd.SELECT)):
			lcd.clear()
			lcd.backlight(lcd.ON)
			ip_address()

