#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import *
from time import sleep, strftime
from datetime import datetime
from Adafruit_I2C import Adafruit_I2C
from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

import smbus

lcd = Adafruit_CharLCDPlate(busnum = 1)
lcd.begin(16,1)

def stuff():
	sleep(1)
	lcd.clear()

while 1:
	lcd.clear()
	lcd.message("RGB DISPLAY")
	stuff()
	lcd.backlight(lcd.RED)
	lcd.message("RED")
	stuff()
	lcd.backlight(lcd.YELLOW)
	lcd.message("Yellow")
	stuff()
	lcd.backlight(lcd.GREEN)
	lcd.message("Green")
	stuff()
	lcd.backlight(lcd.TEAL)
	lcd.message("Teal")
	stuff()
	lcd.backlight(lcd.BLUE)
	lcd.message("Blue")
	stuff()
	lcd.backlight(lcd.VIOLET)
	lcd.message("Violet")
	stuff()
	lcd.backlight(lcd.ON)
	lcd.message("On")
	stuff()
	lcd.backlight(lcd.OFF)
	lcd.message("Off")
	stuff()        
