#!/usr/bin/python

from subprocess import *
from time import sleep, strftime
from datetime import datetime

import smbus
import subprocess
import re
import os


cmd2 = "ip addr show wlan0 | grep inet | awk '{print $2}' | cut -d/ -f1"
#cmd3 = "/sbin/wpa_supplicant -Dwext -iwlan0 -c/etc/wpa_supplicant/wpa_supplicant.conf "
cmd3 = "ifdown wlan0 && ifup wlan0"

# runs the command and does return a value
def run_cmd(cmd):
	p = Popen(cmd, shell=True, stdout=PIPE)
	output = p.communicate()[0]
	return output

# runs the command, but does not return a value
def run_cmd_q(cmd):
	p = Popen(cmd, shell=True, stdout=PIPE)
	output = p.communicate()[0]
	print("Done")

def ip_address():
	wipaddr = run_cmd(cmd2)
	print(wipaddr)
	while wipaddr == "":
		run_cmd_q(cmd3)
		print(wipaddr)
		sleep(5)
		wipaddr = run_cmd(cmd2)
		print(wipaddr)

if __name__ == '__main__':
	print("starting")
	ip_address()
	print("ending")
