#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import *
from time import sleep, strftime
from datetime import datetime
from Adafruit_I2C import Adafruit_I2C
from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

import smbus

lcd = Adafruit_CharLCDPlate(busnum = 1)

cmd = "ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1"
cmd2 = "ip addr show wlan0 | grep inet | awk '{print $2}' | cut -d/ -f1"
name = "hostname"

lcd.begin(16,1)

def run_cmd(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output


	



#def ip_address():
	


#refresh_screen()

if __name__ == '__main__':
	lcd.clear()
    	lcd.message("Adafruit RGB LCD\nPlate w/Keypad!")
    	sleep(5)
    	while 1:
			lcd.clear()
			hostname = run_cmd(name)
			lcd.message(hostname)
			lcd.message(datetime.now().strftime('%b %d  %H:%M:%S\n'))
			sleep(3)
			ipaddr = run_cmd(cmd)
			wipaddr = run_cmd(cmd2)
			lcd.clear()
			if ipaddr == "":
				lcd.message('eth0 disconnect\n')
			else:
				lcd.message('IP %s\n' % ( ipaddr ) )
			if wipaddr == "":
				lcd.message('wlan0 disconnect')
			else:
				lcd.message('WIP %s' % ( wipaddr ) )
