#!/usr/bin/python

# Monitors GPIO Pin 17 to start LCD screen.  Should be CRON'd

from time import sleep
import os
import RPi.GPIO as GPIO
pinnum = 23

GPIO.setmode(GPIO.BCM)
GPIO.setup(pinnum, GPIO.IN)

while True:
        if ( GPIO.input(pinnum) == False ):
		print("Trigger")
                os.system('/usr/bin/python /home/webide/repositories/lcd/bounceRelay.py &')
		sleep(1)
        sleep(1);

